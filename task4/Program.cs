﻿namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Invoice invoice = new(123, "Customer Inc.", "Provider LLC");
            invoice.SetArticle("Product 1");
            invoice.SetQuantity(5);
            double price = 100.0;
            double vatRate = 20.0;

            double priceWithoutVAT = invoice.CalculatePriceWithoutVAT(price);
            double priceWithVAT = invoice.CalculatePriceWithVAT(price, vatRate);

            invoice.Show();
            Console.WriteLine($"\nPrice without VAT: {priceWithoutVAT:C2}");
            Console.WriteLine($"Price with VAT ({vatRate}%): {priceWithVAT:C2}");
        }
    }
}