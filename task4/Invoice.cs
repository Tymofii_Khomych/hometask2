﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class Invoice
    {
        private readonly int account;
        private readonly string customer;
        private readonly string provider;
        private string article;
        private int quantity;

        public Invoice(int account, string customer, string provider)
        {
            this.account = account;
            this.customer = customer;
            this.provider = provider;
        }

        public void SetArticle(string article)
        {
            this.article = article;
        }

        public void SetQuantity(int quantity)
        {
            this.quantity = quantity;
        }

        public double CalculatePriceWithoutVAT(in double price = 100)
        {
            return price * quantity;
        }

        public double CalculatePriceWithVAT(in double price = 100, double vatRate = 20)
        {
            double priceWithoutVAT = CalculatePriceWithoutVAT(price);
            double vat = priceWithoutVAT * vatRate / 100.0;
            return priceWithoutVAT + vat;
        }

        public void Show()
        {
            Console.WriteLine("Account: " + account);
            Console.WriteLine("Customer: " + customer);
            Console.WriteLine("Provider: " + provider);
            Console.WriteLine("Article: " + article);
            Console.WriteLine("Quantity: " + quantity);
        }
    }
}
