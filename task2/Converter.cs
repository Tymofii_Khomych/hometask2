﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Converter
    {
        private double usd;
        private double eur;
        private double rub;

        public Converter(double usd, double eur, double rub)
        {
            this.usd = usd;
            this.eur = eur;
            this.rub = rub;
        }

        public double ConvertToUSD(double uah)
        {
            return uah / usd;
        }

        public double ConvertToEUR(double uah)
        {
            return uah / eur;
        }

        public double ConvertToRUB(double uah)
        {
            return uah / rub;
        }

        public double ConvertFromUSD(double usd)
        {
            return usd * this.usd;
        }

        public double ConvertFromEUR(double eur)
        {
            return eur * this.eur;
        }

        public double ConvertFromRUB(double rub)
        {
            return rub * this.rub;
        }
    }
}
