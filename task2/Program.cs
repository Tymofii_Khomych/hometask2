﻿namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Converter converter = new Converter(36.96, 40.97, 0.45);

            double usd = converter.ConvertToUSD(1000);
            Console.WriteLine($"1000 гривень = {usd:C2} доларiв США");

            double eur = converter.ConvertToEUR(1000);
            Console.WriteLine($"1000 гривень = {eur:C2} євро");

            double rub = converter.ConvertToRUB(1000);
            Console.WriteLine($"1000 гривень = {rub:C2} росiйських рублів");


            double uah = converter.ConvertFromUSD(100);
            Console.WriteLine($"100 доларiв США = {uah:C2} гривень");

            uah = converter.ConvertFromEUR(100);
            Console.WriteLine($"100 євро = {uah:C2} гривень");

            uah = converter.ConvertFromRUB(1000);
            Console.WriteLine($"1000 росiйських рублiв = {uah:C2} гривень");
        }
    }
}