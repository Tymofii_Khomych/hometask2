﻿namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Employee emp = new("Ivanov", "Petro");
            emp.Position = "Programmer";
            emp.Experience = 5;
            emp.CalculateSalary();
        }
    }
}