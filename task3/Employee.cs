﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Employee
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Position { get; set; }
        public int Experience { get; set; }

        public Employee(string lastName, string firstName)
        {
            LastName = lastName;
            FirstName = firstName;
        }

        public void CalculateSalary()
        {
            double salary = 0;
            double tax = 0;

            switch (Position.ToLower())
            {
                case "manager":
                    salary = 1500 + (Experience * 50);
                    break;
                case "programmer":
                    salary = 2000 + (Experience * 70);
                    break;
                case "designer":
                    salary = 1800 + (Experience * 60);
                    break;
                default:
                    Console.WriteLine("Invalid position.");
                    return;
            }

            if (salary < 1000)
            {
                tax = salary * 0.1;
            }
            else if (salary >= 1000 && salary < 2000)
            {
                tax = salary * 0.15;
            }
            else
            {
                tax = salary * 0.2;
            }

            Console.WriteLine($"Employee: {LastName} {FirstName}");
            Console.WriteLine($"Position: {Position}");
            Console.WriteLine($"Salary: {salary:C2}");
            Console.WriteLine($"Tax: {tax:C2}");
        }
    }
}
